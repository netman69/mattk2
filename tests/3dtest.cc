#include <mapplication.h>
#include <mwindow.h>
#include <mtimer.h>

#include <cmath>

#include <stdlib.h> /* exit(), EXIT_SUCCESS */

class Point3D {
	public:
		double x, y, z;
		Point3D();
		Point3D(double x, double y, double z);
		Point3D operator+(Point3D a);
		Point3D operator-(Point3D a);
		Point3D operator*(Point3D a);
		Point3D operator/(Point3D a);
		Point3D operator^(Point3D a); // rotate
};

Point3D::Point3D() {
	this->x = 0;
	this->y = 0;
	this->z = 0;
}

Point3D::Point3D(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

Point3D Point3D::operator+(Point3D a) {
	Point3D result;
	result.x = this->x + a.x;
	result.y = this->y + a.y;
	result.z = this->z + a.z;
	return result;
}

Point3D Point3D::operator-(Point3D a) {
	Point3D result;
	result.x = this->x - a.x;
	result.y = this->y - a.y;
	result.z = this->z - a.z;
	return result;
}

Point3D Point3D::operator*(Point3D a) {
	Point3D result;
	result.x = this->x * a.x;
	result.y = this->y * a.y;
	result.z = this->z * a.z;
	return result;
}

Point3D Point3D::operator/(Point3D a) {
	Point3D result;
	result.x = this->x / a.x;
	result.y = this->y / a.y;
	result.z = this->z / a.z;
	return result;
}

Point3D Point3D::operator^(Point3D a) { /* Rotate */
	Point3D ret;
	ret.x = this->x * cos(a.x) - this->y * sin(a.x);
	ret.y = this->x * sin(a.x) + this->y * cos(a.x);
	ret.z = ret.y * sin(a.y) + this->z * cos(a.y);
	ret.y = ret.y * cos(a.y) - this->z * sin(a.y);
	ret.z = ret.x * sin(a.z) + this->z * cos(a.z);
	ret.x = ret.x * cos(a.z) - this->z * sin(a.z);
	return ret;
}

class MyWindow : public MWindow, public MTimerEvent {
private:
	double off, lt, dt;
	MTimer *timer;

public:
	MyWindow(MApplication *a, int x, int y, unsigned int w, unsigned int h) : MWindow(a, x, y, w, h) {
		off = 0;
		lt = MTimer::get_time();
		dt = 0;
		timer = new MTimer(app, this, 10);
		timer->start();
		invalidate();
	}

	~MyWindow() {
		delete timer;
	}

	void draw(MDrawable *d) {
		unsigned long ct = MTimer::get_time();
		dt = (ct - lt) / 1000.0f;
		lt = ct;
		d->color.set(0, 0, 0);
		d->fill_rect(0, 0, d->w, d->h);
		off += dt;
		Point3D point;
		for (float i = -5; i < 5; i += 0.2) {
			for (float j = -5; j < 5; j += 0.2) {
				point = (Point3D(i, j, sin(i+off*5)) ^ Point3D(off, 0, 0)) * Point3D(200, 1, -200) + Point3D(0, 11, 500);
				double m = 1-(point.y-6)/13;
				d->color.set((int)(double)((sin(i+off*10)*100+127)*m), (int)(double)((127-sin(i+off*10)*100)*m), (int)(double)((cos(i+off*10)*100+127)*m));
				d->draw_point(d->w / 2 + (int) (point.x / point.y), d->h / 2 + (int) (point.z / point.y));
			}
		}
	}

	void timer_event() {
		invalidate();
	}

	void on_close(bool irrevocable) {
		exit(EXIT_SUCCESS);
	}
};

int mat_main(int argc, char *argv[]) {
	MApplication a(argc, argv);
	MyWindow w(&a, 0, 0, 640, 480);
	w.map();
	return a.run();
}
