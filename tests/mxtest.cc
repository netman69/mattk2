#include <mapplication.h>
#include <mwindow.h>
#include <mfont.h>

#include <math.h> /* sin(), cos() */
#include <stdlib.h> /* exit(), EXIT_SUCCESS */

#include <iostream>

MColor bgcolor(0, 0, 0);
MWindow *win;

class MyWindow : public MWindow {
public:
	MyWindow(MApplication *a, int x, int y, unsigned int w, unsigned int h) : MWindow(a, x, y, w, h) {};

	void on_close(bool irrevocable) {
		exit(EXIT_SUCCESS);
	}
};

class MyWidget : public MWidget {
public:
	explicit MyWidget(MLayout *layout) : MWidget(layout) {};

	void draw(MDrawable *d) {
		d->color.set(&bgcolor);
		d->fill_rect(0, 0, d->w, d->h);
		d->color.set(0xFF, 0, 0);
		d->draw_line_aa(1, 1, d->w - 2, d->h - 2);
		d->draw_line_aa(1, d->h - 2, d->w - 2, 1);

		d->color.set(0, 0, 0xFF);
		d->draw_rect(40, 40, d->w - 80, d->h - 80);

		d->color.set(0, 0xFF, 0);
		d->fill_rect(d->w - 50, d->h - 50, 40, 40);

		d->color.set(0xFF, 0, 0xFF/*, 0x80*/);
		d->fill_rect(80, 80, d->w - 160, d->h - 160);
		d->color.set(0, 0xFF, 0xFF/*, 0x80*/);
		d->fill_rect(120, 120, d->w - 240, d->h - 240);
		d->color.set(0xFF, 0xFF, 0xFF);
		d->draw_line_aa(10, 10, d->w - 10, 10);
		d->draw_line_aa(d->w - 10, 10, d->w - 10, d->h - 10);
		d->draw_line_aa(d->w - 10, d->h - 10, 10, d->h - 10);
		d->draw_line_aa(10, d->h - 10, 10, 10);

		d->draw_line_aa(15, 15, 15, d->h - 15);
		d->draw_line_aa(15, d->h - 15, d->w - 15, d->h - 15);
		d->draw_line_aa(d->w - 15, d->h - 15, d->w - 15, 15);
		d->draw_line_aa(d->w - 15, 15, 15, 15);

		d->draw_line(20, 20, d->w - 20, 20);
		d->draw_line(d->w - 20, 20, d->w - 20, d->h - 20);
		d->draw_line(d->w - 20, d->h - 20, 20, d->h - 20);
		d->draw_line(20, d->h - 20, 20, 20);

		d->draw_line(25, 25, 25, d->h - 25);
		d->draw_line(25, d->h - 25, d->w - 25, d->h - 25);
		d->draw_line(d->w - 25, d->h - 25, d->w - 25, 25);
		d->draw_line(d->w - 25, 25, 25, 25);

		d->fill_rect(-100, -100, 150, 150);
		d->fill_rect(d->w - 20, d->h - 20, 70, 70);

		double px = d->w / 2.0 + sin(0.0) * 100.0, py = d->h / 2.0 + cos(0.0) * 100.0;
		for (double i = 0.0; i < 2.0 * M_PI + M_PI / 40; i += M_PI / 40) {
			double x = d->w / 2.0 + sin(i) * 100.0, y = d->h / 2.0 + cos(i) * 100.0;
			d->draw_line_aa(px, py, x, y);
			px = x, py = y;
		}
		px = d->w / 2.0 + sin(0.0) * 125.0, py = d->h / 2.0 + cos(0.0) * 125.0;
		for (double i = 0.0; i < 2.0 * M_PI + M_PI / 5; i += M_PI / 5) {
			double x = d->w / 2.0 + sin(i) * 125.0, y = d->h / 2.0 + cos(i) * 125.0;
			d->draw_line_aa(px, py, x, y);
			px = x, py = y;
		}
		px = d->w / 2.0 + sin(0.0) * 150.0, py = d->h / 2.0 + cos(0.0) * 150.0;
		for (double i = 0.0; i < 2.0 * M_PI + M_PI / 30; i += M_PI / 30) {
			double x = d->w / 2.0 + sin(i) * 150.0, y = d->h / 2.0 + cos(i) * 150.0;
			d->draw_line(px, py, x, y);
			px = x, py = y;
		}

		MFont font;
		font.load("fonts/vera/Vera.ttf");
		font.set_size(20);
		int p = d->h / 2 + font.ascent - (font.ascent - font.descent) / 2;
		d->draw_line(0, p, d->w, p);
		d->draw_line(0, p - font.ascent, d->w, p - font.ascent);
		d->draw_line(0, p - font.descent, d->w, p - font.descent);
		int w = font.draw_text_width("Mat is cool! Qqjgp.");

		d->color.set(0x50, 0x50, 0x50, 0xA0);
		d->fill_rect(d->w / 2 - w / 2, d->h / 2 - font.height / 2 - 1, w, font.height + 2);

		d->color.set(0x0, 0x0, 0x0);
		font.draw_text(d, "Mat is cool! Qqjgp.", d->w / 2 - w / 2, p);
		font.set_size(15);
		w = font.draw_text_width("Mat is cool!");
		font.draw_text(d, "Mat is cool!", d->w / 2 - w / 2, p + 20);
		font.load("fonts/gohu/gohufont-11b.bdf");
		w = font.draw_text_width("Mat is cool!");
		font.draw_text(d, "Mat is cool!", d->w / 2 - w / 2, p + 35);

		MDrawableFormat f;
		MDrawable sub(100, 50, &f);
		sub.color.set(0, 0, 0, 0);
		sub.clear();
		draw_sub(sub);
		d->color.set_a(0xFF);
		d->draw(&sub, 15, 15);
		MDrawable sub2(d, 30, 30, 100, 50);
		draw_sub(sub2);
	}

	void draw_sub(MDrawable &sub) {
		sub.color.set(0xFF, 0x50, 0x00, 0x80);
		sub.fill_rect(0, 0, sub.w, sub.h);
		sub.color.set(0xFF, 0xFF, 0xFF);
		sub.draw_line_aa(1, 1, sub.w - 2, sub.h - 2);
		sub.draw_line_aa(1, sub.h - 2, sub.w - 2, 1);
	}
};

class MyButton : public MWidget {
public:
	bool button_pressed;

	explicit MyButton(MLayout *layout) : MWidget(layout) {
		button_pressed = false;
	};

	void draw(MDrawable *d) {
		if (button_pressed && pointer_inside)
			d->color.set(0x50, 0x50, 0x50);
		else if (pointer_inside)
			d->color.set(0xFF, 0xFF, 0xFF);
		else d->color.set(0x88, 0x88, 0x88);
		d->clear();
		d->color.set(0, 0, 0);
		MFont font;
		font.load("fonts/vera/Vera.ttf");
		font.set_size(15);
		int p = d->h / 2 + font.ascent - (font.ascent - font.descent) / 2;
		int w = font.draw_text_width("Button");
		font.draw_text(d, "Button", d->w / 2 - w / 2, p);
		d->color.set(0xAA, 0xAA, 0xAA);
		d->draw_rect(0, 0, d->w - 1, d->h - 1);
	}

	void on_pointer_enter() {
		invalidate();
	}

	void on_pointer_exit() {
		invalidate();
	}

	void on_button_press(int button) {
		if (button == 1)
			button_pressed = true;
		invalidate();
	}

	void on_button_release(int button) {
		if (button == 1 && pointer_inside && button_pressed) {
			bgcolor.r += 0x55;
			bgcolor.b -= 0x43;
			bgcolor.g += 0x81;
			win->invalidate();
		}
		if (button == 1)
			button_pressed = false;
		invalidate();
	}
};

int mat_main(int argc, char *argv[]) {
	MApplication a(argc, argv);
	MyWindow w(&a, 0, 0, 480, 480);
	MyWidget widget(&w);
	win = &w;
	widget.pos.w = 480;
	widget.pos.h = 410;
	widget.anchors = MAnchor::BOTTOM | MAnchor::TOP | MAnchor::RIGHT | MAnchor::LEFT;

	MyButton btn(&w);
	btn.pos.x = 360;
	btn.pos.y = 430;
	btn.pos.w = 100;
	btn.pos.h = 30;
	btn.anchors = MAnchor::BOTTOM | MAnchor::RIGHT;

	w.map();
	return a.run();
}
