#include "mdrawable.h"
#include "merror.h"

#include <stdlib.h> /* NULL, abs() */
#include <string.h> /* memset() */

MRect::MRect() {
	x = 0;
	y = 0;
	w = 0;
	h = 0;
}

MRect::MRect(int x, int y, int w, int h) {
	this->x = x;
	this->y = y;
	this->w = w;
	this->h = h;
}

MRect::MRect(MRect *rect) {
	x = rect->x;
	y = rect->y;
	w = rect->w;
	h = rect->h;
}

void MRect::trim_inversion() {
	if (w < 0)
		w = 0;
	if (h < 0)
		h = 0;
}

void MRect::fit(MRect *rect) {
	if (x < rect->x) {
		w -= rect->x - x;
		x = rect->x;
	}
	if (y < rect->y) {
		h -= rect->y - y;
		y = rect->y;
	}
	w = ((x + w > rect->x + rect->w) ? rect->x + rect->w - x : w);
	h = ((y + h > rect->y + rect->h) ? rect->y + rect->h - y : h);
	if (w < 0)
		w = 0;
	if (h < 0)
		h = 0;
}

void MRect::move(int x, int y) {
	this->x = x;
	this->y = y;
}

void MRect::translate(int x, int y) {
	this->x += x;
	this->y += y;
}

bool MRect::is_inverted() {
	return w < 0 || h < 0;
}

bool MRect::is_inside(int x, int y) {
	return x >= this->x && y >= this->y && x < this->x + w && y < this->y + h;
}

bool MRect::has_size() {
	return w > 0 && h > 0;
}

int MRect::x1() {
	return x + w;
}

int MRect::y1() {
	return y + h;
}


MDrawable::MDrawable(unsigned int w, unsigned int h, MDrawableFormat *f, bool alloc) : format(f) {
	this->pitch = ((format.pitch == 0) ? w * format.bpp : format.pitch);
	this->w = w;
	this->h = h;
	if (alloc) {
		data = new unsigned char [pitch * h];
		data_owned = true;
	} else data_owned = false;
	color.set_format(format.color_format);
	clip = MRect(0, 0, w, h);
}

MDrawable::MDrawable(MDrawable *d, int x, int y, int w, int h) {
	MRect rect(x, y, w, h);
	init(d, &rect);
}

MDrawable::MDrawable(MDrawable *d, MRect *rect) {
	init(d, rect);
}

void MDrawable::init(MDrawable *d, MRect *rect) {
	clip = MRect(rect);
	clip.trim_inversion();
	clip.fit(&d->clip);
	clip.move(clip.x - rect->x, clip.y - rect->y);
	w = rect->w;
	h = rect->h;
	format = MDrawableFormat(d->format);
	format.offset_s += rect->x * format.bpp;
	pitch = d->pitch;
	data = d->data + rect->y * format.pitch;
	data_owned = false;
	color.set_format(format.color_format);
}

MDrawable::~MDrawable() {
	if (data_owned)
		delete [] data;
}

void MDrawable::draw(MDrawable *d, int x, int y) {
	MRect src(&d->clip);
	MRect dst(&d->clip);
	dst.translate(x, y); /* We want the clipping rect of src, but moved to the right spot. */
	dst.fit(&clip); /* And clipped by our own clipping rect. */
	dst.translate(-x, -y);
	src.fit(&dst); /* But we also want the source to match that clipping. */
	dst.translate(x, y);
	int sx = dst.x - src.x;
	int sy = dst.x - src.x;
	MColor tc(&color);
	int off;
	for (y = dst.y; y < dst.y1(); ++y) {
		for (x = dst.x; x < dst.x1(); ++x) {
			off = d->format.offset_s + (x - sx) * d->format.bpp + (y - sy) * d->pitch;
			color.read(*((unsigned int *)(((unsigned char *) d->data) + off)), d->color.format);
			draw_point_pm(x, y);
		}
	}
	color.set(&tc);
}

void MDrawable::draw_point_na(int x, int y) {
	if (!clip.is_inside(x, y))
		return;
	int off = format.offset_s + x * format.bpp + y * pitch;
	switch (format.bpp) {
		case 4:
			*((unsigned int *)(((unsigned char *) data) + off)) = color.value;
			break;
		case 3:
			*((unsigned int *)(((unsigned char *) data) + off)) &= ~format.mask;
			*((unsigned int *)(((unsigned char *) data) + off)) |= color.value;
			break;
		case 2:
			*((unsigned short *)(((unsigned char *) data) + off)) = color.value;
			break;
		default:
			throw new MError("unknown color format");
	}
}

void MDrawable::draw_point(int x, int y) {
	if (color.a == 0xFF) {
		draw_point_na(x, y);
		return;
	}
	MColor mc;
	mc.set_format(format.color_format);
	if (!clip.is_inside(x, y))
		return;
	int off = format.offset_s + x * format.bpp + y * pitch;
	switch (format.bpp) {
		case 4:
			{
				unsigned int *addr = (unsigned int *)(((unsigned char *) data) + off);
				mc.read(*addr);
				mc.blend(&color);
				*addr = mc.value;
			}
			break;
		case 3:
			{
				unsigned int *addr = (unsigned int *)(((unsigned char *) data) + off);
				mc.read(*addr);
				mc.blend(&color);
				*addr &= ~format.mask;
				*addr |= mc.value;
			}
			break;
		case 2:
			{
				unsigned short *addr = (unsigned short *)(((unsigned char *) data) + off);
				mc.read((unsigned int) *addr);
				mc.blend(&color);
				*addr &= ~format.mask;
				*addr |= mc.value;
			}
			break;
		default:
			throw new MError("unknown color format");
	}
}

void MDrawable::draw_point_pm(int x, int y) {
	if (color.a == 0xFF) {
		draw_point_na(x, y);
		return;
	}
	MColor mc;
	mc.set_format(format.color_format);
	if (!clip.is_inside(x, y))
		return;
	int off = format.offset_s + x * format.bpp + y * pitch;
	switch (format.bpp) {
		case 4:
			{
				unsigned int *addr = (unsigned int *)(((unsigned char *) data) + off);
				mc.read(*addr);
				mc.blend_pm(&color);
				*addr = mc.value;
			}
			break;
		case 3:
			{
				unsigned int *addr = (unsigned int *)(((unsigned char *) data) + off);
				mc.read(*addr);
				mc.blend_pm(&color);
				*addr &= ~format.mask;
				*addr |= mc.value;
			}
			break;
		case 2:
			{
				unsigned short *addr = (unsigned short *)(((unsigned char *) data) + off);
				mc.read((unsigned int) *addr);
				mc.blend_pm(&color);
				*addr &= ~format.mask;
				*addr |= mc.value;
			}
			break;
		default:
			throw new MError("unknown color format");
	}
}

void MDrawable::draw_line(int x0, int y0, int x1, int y1) {
	int dx = abs(x1 - x0); /* This is an implementation of bresenham's line algorithm. */
	int dy = abs(y1 - y0);
	int sx = ((x0 <= x1) ? 1 : -1);
	int sy = ((y0 <= y1) ? 1 : -1);
	int x = 0, y = 0;
	if (dx >= dy) {
		int e = 2 * dy - dx;
		for (; x <= dx; ++x) {
			draw_point(x0 + x * sx, y0 + y * sy);
			if (e >= 0) {
				++y;
				e -= 2 * dx;
			}
			e += 2 * dy;
		}
	} else {
		int e = 2 * dx - dy;
		for (; y <= dy; ++y) {
			draw_point(x0 + x * sx, y0 + y * sy);
			if (e >= 0) {
				++x;
				e -= 2 * dy;
			}
			e += 2 * dx;
		}
	}
}

void MDrawable::draw_line_aa(int x0, int y0, int x1, int y1) {
	unsigned char ta = color.a; /* Save alpha to restore later. */
	int dx = abs(x1 - x0);
	int dy = abs(y1 - y0);
	int sx = ((x0 <= x1) ? 1 : -1);
	int sy = ((y0 <= y1) ? 1 : -1);
	int x = 0, y = 0;
	if (dx >= dy) {
		int em = (dx == 0) ? 1 : 2 * dx; /* Error equals 0-1 fractional error multiplied by this. */
		int eo = 2 * dy - em; /* Error accumulator e has this value if there is 0 error. */
		int e = eo; /* This e and eo should be always a negative value or 0, e becomes 0 if we have 1px error while in above function it does at 0.5px already. */
		for (; x <= dx; ++x) { /* We go until 1px beyond the destination to have the last pixel properly drawn. */
			color.set_a((int) 0xFF + (eo - e) * ta / em);
			draw_point(x0 + x * sx, y0 + y * sy);
			color.set_a(0xFF - color.a);
			draw_point(x0 + x * sx, y0 + (y + 1) * sy);
			if (e >= 0) {
				++y;
				e -= 2 * dx; /* These additions and subtractions combine to 0 if there is no error. */
			}
			e += 2 * dy; /* Thus leaving e to be the initial value at such point. */
		}
	} else {
		int em = (dy == 0) ? 1 : 2 * dy;
		int eo = 2 * dx - em;
		int e = eo;
		for (; y <= dy; ++y) {
			color.set_a((int) 0xFF + (eo - e) * ta / em);
			draw_point(x0 + x * sx, y0 + y * sy);
			color.set_a(0xFF - color.a);
			draw_point(x0 + (x + 1) * sx, y0 + y * sy);
			if (e >= 0) {
				++x;
				e -= 2 * dy;
			}
			e += 2 * dx;
		}
	}
	color.set_a(ta);
}

void MDrawable::draw_rect(int x, int y, int w, int h) {
	MRect rect(x, y, w, h);
	draw_rect(&rect);
}

void MDrawable::draw_rect(MRect *rect) {
	if (rect->is_inverted())
		return;
	fill_rect(rect->x, rect->y, rect->w, 1); /* fill_rect is fast horizontally. */
	fill_rect(rect->x, rect->y1(), rect->w, 1);
	draw_line(rect->x, rect->y, rect->x, rect->y1());
	draw_line(rect->x1(), rect->y, rect->x1(), rect->y1());
}

void MDrawable::clear() {
	fill_rect(&clip, true);
}

void MDrawable::fill(bool overwrite_alpha) {
	fill_rect(&clip, overwrite_alpha);
}

void MDrawable::fill_rect(int x, int y, int w, int h, bool overwrite_alpha) {
	MRect rect(x, y, w, h);
	fill_rect(&rect);
}

void MDrawable::fill_rect(MRect *rect, bool overwrite_alpha) {
	MRect r = *rect;
	if (r.is_inverted())
		return;
	r.fit(&clip);
	if (color.a != 0xFF && !overwrite_alpha) {
		fill_rect_alpha(r.x, r.y, r.w, r.h);
		return;
	}
	int off, x = r.x, y = r.y;
	switch (format.bpp) {
		case 4:
			for (int oy = y; y < oy + r.h; ++y) {
				off = format.offset_s + x * format.bpp + y * pitch;
				#if defined(IS_AMD64) || defined(IS_X86)
					__asm volatile("cld; rep stosl" : : "a"(color.value), "c"(r.w), "D"(((unsigned char *) data) + off) : "memory");
				#else
					for (int i = x; i < x + r.w; ++i, off += 4)
						*((unsigned int *)(((unsigned char *) data) + off)) = color.value;
				#endif
			}
			break;
		case 3:
			for (int oy = y; y < oy + r.h; ++y) {
				off = format.offset_s + x * format.bpp + y * pitch;
				for (int i = x; i < x + r.w; ++i, off += 3) {
					*((unsigned int *)(((unsigned char *) data) + off)) &= ~format.mask;
					*((unsigned int *)(((unsigned char *) data) + off)) |= color.value;
				}
			}
			break;
		case 2:
			for (int oy = y; y < oy + r.h; ++y) {
				off = format.offset_s + x * format.bpp + y * pitch;
				#if defined(IS_AMD64) || defined(IS_X86)
					__asm volatile("cld; rep stosw" : : "a"(color.value), "c"(r.w), "D"(((unsigned char *) data) + off) : "memory");
				#else
					for (int i = x; i < x + r.w; ++i, off += 2)
						*((unsigned short *)(((unsigned char *) data) + off)) = color.value;
				#endif
			}
			break;
		default:
			throw new MError("unknown color format");
	}
}

void MDrawable::fill_rect_alpha(int x, int y, int w, int h) {
	MColor mc;
	mc.set_format(format.color_format);
	int off;
	switch (format.bpp) {
		case 4:
			for (int oy = y; y < oy + h; ++y) {
				off = format.offset_s + x * format.bpp + y * pitch;
				for (int i = x; i < x + w; ++i, off += 4) {
					unsigned int *addr = (unsigned int *)(((unsigned char *) data) + off);
					mc.read(*addr);
					mc.blend(&color);
					*addr = mc.value;
				}
			}
			break;
		case 3:
			for (int oy = y; y < oy + h; ++y) {
				off = format.offset_s + x * format.bpp + y * pitch;
				for (int i = x; i < x + w; ++i, off += 3) {
					unsigned int *addr = (unsigned int *)(((unsigned char *) data) + off);
					mc.read(*addr);
					mc.blend(&color);
					*addr &= ~format.mask;
					*addr |= mc.value;
				}
			}
			break;
		case 2:
			for (int oy = y; y < oy + h; ++y) {
				off = format.offset_s + x * format.bpp + y * pitch;
				unsigned short *addr = (unsigned short *)(((unsigned char *) data) + off);
				mc.read((unsigned int) *addr);
				mc.blend(&color);
				*addr = mc.value;
			}
			break;
		default:
			throw new MError("unknown color format");
	}
}
