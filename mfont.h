#ifndef __MFONT_H__
#define __MFONT_H__

#include <string>

#include <ft2build.h>
#include FT_FREETYPE_H

class MDrawable;

class MFont {
public:
	int ascent, descent, height;

	MFont();
	~MFont();

	void load(void *buffer, int len);
	void load(std::string filename);
	void set_size(int height);
	void draw_text(MDrawable *d, std::string str, int x, int y);
	int draw_text_width(std::string str);

private:
	FT_Library lib;
	FT_Face face;

	void init_font();
	void draw_char(MDrawable *d, char c, int x, int y);
};

#endif /* __MFONT_H__ */
