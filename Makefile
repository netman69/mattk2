# Useful cxxflags are:
#  -DIS_AMD64, -DIS_X86, -DNO_SHM

CXXFLAGS += -g -DIS_AMD64 -I/usr/include/freetype2 -I. -Wall
LDFLAGS += -g -lX11 -lXext -lfreetype
OBJS_XLIB = xlib/mapplication.o xlib/mwindow.o
OBJS = merror.o mcolor.o mtimer.o mdrawable.o mdrawableformat.o mfont.o mwidget.o mlayout.o
OBJS_3DTEST = tests/3dtest.o
OBJS_MXTEST = tests/mxtest.o

all: mxtest 3dtest

font_builtin.c:
	cd fonts
	@#bdftopcf fonts/gohu/gohufont-11.bdf  > font_builtin
	cp fonts/vera/VeraMono.ttf font_builtin
	xxd -i font_builtin font_builtin.c

mxtest: $(OBJS) $(OBJS_MXTEST) $(OBJS_XLIB)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS) $(OBJS_MXTEST) $(OBJS_XLIB)

3dtest: $(OBJS) $(OBJS_3DTEST) $(OBJS_XLIB)
	$(CXX) $(LDFLAGS) -o $@ $(OBJS) $(OBJS_3DTEST) $(OBJS_XLIB)

mfont.o: font_builtin.c

clean:
	rm -rf $(OBJS) $(OBJS_MXTEST) $(OBJS_3DTEST) $(OBJS_XLIB) mxtest 3dtest
	rm -rf font_builtin font_builtin.c
