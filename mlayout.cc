#include "mlayout.h"

#include <algorithm>

MLayoutButtonCapture::MLayoutButtonCapture(int button, MWidget *w) {
	this->button = button;
	this->w = w;
}

void MLayout::on_pointer_move(int x, int y) {
	for(std::list<MWidget *>::iterator i = widgets.begin(); i != widgets.end(); ++i) {
		MWidget *w = *i;
		if (w->pos.is_inside(x, y))
			w->on_pointer_move(x, y);
		if (w->pos.is_inside(x, y) && w->pointer_inside == false) {
			w->pointer_inside = true;
			w->on_pointer_enter();
		}
		if (!w->pos.is_inside(x, y) && w->pointer_inside == true) {
			w->pointer_inside = false;
			w->on_pointer_exit();
		}
	}
}

void MLayout::on_button_press(int button) {
	for(std::list<MWidget *>::iterator i = widgets.begin(); i != widgets.end(); ++i) {
		if ((*i)->pointer_inside) {
			(*i)->on_button_press(button);
			captures.push_back(MLayoutButtonCapture(button, *i));
		}
	}
}

void MLayout::on_button_release(int button) {
	for (unsigned int i = 0; i < captures.size(); ++i) {
		MLayoutButtonCapture c = captures.at(i);
		if (c.button == button) {
			//if (*std::find(widgets.begin(), widgets.end(), c.w) == c.w)
				c.w->on_button_release(button);
			captures.erase(captures.begin() + i);
			return;
		}
	}
	for(std::list<MWidget *>::iterator i = widgets.begin(); i != widgets.end(); ++i) {
		if ((*i)->pointer_inside)
			(*i)->on_button_release(button);
	}
}

void MLayout::moveresize(MRect *newpos) {
	MRect ipos(0, 0, newpos->w, newpos->h);
	for(std::list<MWidget *>::iterator i = widgets.begin(); i != widgets.end(); ++i) {
		MWidget *w = *i;
		if (w->anchors & MAnchor::RIGHT) {
			if (w->anchors & MAnchor::LEFT)
				w->pos.w += newpos->w - pos.w;
			else w->pos.x += newpos->w - pos.w;
		}
		if (w->anchors & MAnchor::BOTTOM) {
			if (w->anchors & MAnchor::TOP)
				w->pos.h += newpos->h - pos.h;
			else w->pos.y += newpos->h - pos.h;
		}
	}
	MWidget::moveresize(newpos);
}

void MLayout::draw(MDrawable *d) {
	MRect rect(0, 0, d->w, d->h);
	draw(d, &rect);
}

void MLayout::draw(MDrawable *d, MRect *rect) {
	for(std::list<MWidget *>::iterator i = widgets.begin(); i != widgets.end(); ++i) {
		MWidget *w = *i;
		MRect overlap = w->pos;
		overlap.fit(rect);
		if (overlap.has_size()) {
			MDrawable nd(d, &w->pos);
			overlap.move(-w->pos.x, -w->pos.y);
			w->draw(&nd, &overlap);
		}
	}
}
