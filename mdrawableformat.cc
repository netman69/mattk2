#include "mdrawableformat.h"
#include "merror.h"

int cf_bytes_per_pixel(int colorformat) {
	switch (colorformat) {
		case MColorFormat::RGBA8888:
		case MColorFormat::ARGB8888:
		case MColorFormat::BGRA8888:
		case MColorFormat::ABGR8888:
			return 4;
		case MColorFormat::RGB888:
		case MColorFormat::BGR888:
		case MColorFormat::RGB888L:
		case MColorFormat::BGR888L:
			return 3;
		case MColorFormat::RGB565:
		case MColorFormat::BGR565:
			return 2;
		default:
			throw new MError("unknown color format");
	}
}

unsigned int cf_mask(int colorformat) {
	switch (colorformat) {
		case MColorFormat::RGBA8888:
		case MColorFormat::BGRA8888:
			return 0xFFFFFF00;
		case MColorFormat::ARGB8888:
		case MColorFormat::ABGR8888:
			return 0x00FFFFFF;
		case MColorFormat::RGB888:
		case MColorFormat::BGR888:
			return 0x00FFFFFF;
		case MColorFormat::RGB888L:
		case MColorFormat::BGR888L:
			return 0xFFFFFF00;
		case MColorFormat::RGB565:
		case MColorFormat::BGR565:
			return 0x0000FFFF;
		default:
			throw new MError("unknown color format");
	}
}

MDrawableFormat::MDrawableFormat(int colorformat, int offset_s, int pitch) {
	this->color_format = colorformat;
	this->bpp = cf_bytes_per_pixel(colorformat);
	this->mask = cf_mask(colorformat);
	this->offset_s = offset_s;
	this->pitch = pitch;
}

MDrawableFormat::MDrawableFormat(MDrawableFormat *f) {
	color_format = f->color_format;
	offset_s = f->offset_s;
	pitch = f->pitch;
	bpp = f->bpp;
	mask = f->mask;
}
