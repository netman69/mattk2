#ifndef __MCOLOR_H__
#define __MCOLOR_H__

#include "mdrawableformat.h"

class MColor {
public:
	unsigned char r, g, b, a; /* Set format and use set() to make value update automatically. */
	unsigned int value;
	int format;

	MColor();
	MColor(unsigned char r, unsigned char g, unsigned char b);
	MColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	explicit MColor(MColor *color);

	void set_format(int format);

	void set(unsigned char r, unsigned char g, unsigned char b);
	void set(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
	void set(MColor *color);
	void set_a(unsigned char a);

	void blend(MColor *color);
	void blend_pm(MColor *color); /* For pre-multiplied alpha. */

	unsigned int get(int format); /* Gets value for particular color format. */
	void read(unsigned int c); /* Set color according to data in set format. */
	void read(unsigned int c, int format); /* Set color according to data in given format. */
};

#endif /* __MCOLOR_H__ */

