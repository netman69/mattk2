#ifndef __MWIDGET_H__
#define __MWIDGET_H__

#include "mdrawable.h"

#include <stdlib.h> /* NULL */

class MLayout;

namespace MAnchor {
	enum MAnchor {
		NONE   = 0,
		TOP    = 1 << 0,
		LEFT   = 1 << 1,
		BOTTOM = 1 << 2,
		RIGHT  = 1 << 3
	};
}

class MWidget {
public:
	MLayout *layout;
	MRect pos;
	int anchors;
	bool pointer_inside;

	explicit MWidget(MLayout *layout = NULL);
	~MWidget();

	virtual void on_pointer_move(int x, int y) {};
	virtual void on_button_press(int button) {};
	virtual void on_button_release(int button) {};
	virtual void on_pointer_enter() {};
	virtual void on_pointer_exit() {}

	virtual void moveresize(MRect *newpos);
	virtual void moveresize(int x, int y, int w, int h);

	virtual void draw(MDrawable *d); /* Must implement one of these two. */
	virtual void draw(MDrawable *d, MRect *rect);

	virtual void invalidate();
	virtual void invalidate(MRect *rect);
	virtual void invalidate(int x, int y, int w, int h);
};

#endif /* __MWIDGET_H__ */
