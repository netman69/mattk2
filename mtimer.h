#ifndef __MTIMER_H__
#define __MTIMER_H__

class MApplication;

class MTimerEvent {
public:
	virtual void timer_event() {};
};

class MTimer {
private:
	MApplication *app;
	MTimerEvent *tev;

public:
	unsigned long duration, started;
	bool repeat;

	MTimer(MApplication *a, MTimerEvent *tev, unsigned long duration);
	MTimer(MApplication *a, MTimerEvent *tev, unsigned long duration, bool repeat);
	~MTimer();

	static unsigned long get_time();
	void start();
	void stop();
	void trigger();
};

#endif /* __MTIMER_H__ */
