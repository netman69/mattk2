#ifndef __MAPPLICATION_H__
#define __MAPPLICATION_H__

class MTimer;

#include <list>

extern int mat_main(int argc, char *argv[]);

class MApplicationImpl;

class MApplication {
public:
	MApplicationImpl *impl;
	std::list<MTimer *> timers;

	MApplication(int argc, char *argv[]);
	~MApplication();

	int run();
};

#endif /* __MAPPLICATION_H__ */
