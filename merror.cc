#include "merror.h"

#include <string>
#include <iostream>

MError::MError(std::string msg) : message(msg) {
	std::cerr << "error: " << message << "\n";
}
