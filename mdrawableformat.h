#ifndef __MDRAWABLEFORMAT_H__
#define __MDRAWABLEFORMAT_H__

namespace MColorFormat {
	enum MColorFormat {
		NONE,
		RGBA8888,
		ARGB8888,
		BGRA8888,
		ABGR8888,
		RGB888,
		BGR888,
		RGB888L, /* Left-aligned RGB/BGR for use on little-endian systems. */
		BGR888L,
		RGB565,
		BGR565,
	};
}

class MDrawableFormat {
public:
	int color_format;
	int offset_s; /* Offset in line before first pixel, in bytes. */
	int pitch; /* Length of a line in bytes, including padding. */
	int bpp; /* Bytes per pixel. */
	unsigned int mask; /* Mask for RGB values. */

	MDrawableFormat(int colorformat = MColorFormat::RGBA8888, int offset_s = 0, int pitch = 0); /* 0 pitch means pitch based on width */
	explicit MDrawableFormat(MDrawableFormat *f);
};

#endif /* __MDRAWABLEFORMAT_H__ */
