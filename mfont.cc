#include "mfont.h"
#include "mdrawable.h"
#include "merror.h"

#include <stdlib.h> /* NULL */

#include "font_builtin.c"

MFont::MFont() {
	height = 12;
	face = NULL;
	if (FT_Init_FreeType(&lib))
		throw new MError("failed to initialize freetype2");
	load(font_builtin, font_builtin_len);
}

MFont::~MFont() {
	if (face != NULL) {
		FT_Done_Face(face);
		face = NULL;
	}
	FT_Done_FreeType(lib);
}

void MFont::load(void *buffer, int len) {
	if (face != NULL) {
		FT_Done_Face(face);
		face = NULL;
	}
	if (FT_New_Memory_Face(lib, font_builtin, font_builtin_len, 0, &face))
		throw new MError("failed to load font");
	init_font();
}

void MFont::load(std::string filename) {
	if (face != NULL) {
		FT_Done_Face(face);
		face = NULL;
	}
	if (FT_New_Face(lib, filename.c_str(), 0, &face))
		throw new MError("failed to load font");
	init_font();
}

void MFont::init_font() {
	if (face->face_flags & FT_FACE_FLAG_SCALABLE) {
		set_size(height);
	} else if (face->available_sizes != NULL) {
		if (FT_Select_Size(face, 0))
			throw new MError("failed to set font size");
		ascent = face->size->metrics.ascender / 64;
		descent = face->size->metrics.descender / 64;
		height = face->size->metrics.height / 64;
	} else throw new MError("failed to set font size");
}

void MFont::set_size(int height) {
	if (FT_Set_Char_Size(face, 0, height * 64, 0, 0))
		throw new MError("failed to set font size");
	ascent = face->size->metrics.ascender / 64;
	descent = face->size->metrics.descender / 64;
	this->height = face->size->metrics.height / 64;
}

void MFont::draw_char(MDrawable *d, char c, int x, int y) {
	FT_UInt glyph_index = FT_Get_Char_Index(face, c);
	/* With the BDF font i was testing, I somehow always get glyph_index of 0.
	 * This is most likely because the encoding is "winroman".
	 * A solution is here https://www.freetype.org/freetype2/docs/tutorial/step1.html#section-4
	 */
	if (FT_Load_Glyph(face, glyph_index, FT_LOAD_RENDER))
		throw new MError("failed to load glyph ");
	FT_Bitmap *bmp = &face->glyph->bitmap;
	if (face->glyph->format != FT_GLYPH_FORMAT_BITMAP)
		throw MError("problem");
	unsigned char ta = d->color.a; /* Temporary storage for alpha. */
	switch (bmp->pixel_mode) {
		case FT_PIXEL_MODE_MONO:
			for (unsigned int yo = 0; yo < (unsigned) bmp->rows; ++yo) {
				for (unsigned int xo = 0; xo < (unsigned) bmp->width; ++xo) {
					int off = yo * bmp->pitch * 8 + xo;
					if(bmp->buffer[off / 8] & (0x80 >> (off % 8)))
						d->draw_point(x + xo + face->glyph->bitmap_left, y + yo - face->glyph->bitmap_top);
				}
			}
			break;
		case FT_PIXEL_MODE_GRAY:
			if (bmp->num_grays != 256)
				throw MError("unsupported font format");
			for (unsigned int yo = 0; yo < (unsigned) bmp->rows; ++yo) {
				for (unsigned int xo = 0; xo < (unsigned) bmp->width; ++xo) {
					d->color.set_a(bmp->buffer[yo * bmp->pitch + xo]);
					if (d->color.a != 0)
						d->draw_point(x + xo + face->glyph->bitmap_left, y + yo - /*face->glyph->bitmap_top - */face->glyph->metrics.horiBearingY / 64);
				}
			}
			break;
		default:
			throw MError("unsupported font format");
	}
	d->color.set_a(ta);
}

void MFont::draw_text(MDrawable *d, std::string str, int x, int y) {
	for(unsigned int i = 0; i < str.size(); ++i) {
		draw_char(d, str[i], x, y);
		x += face->glyph->advance.x >> 6;
	}
}

int MFont::draw_text_width(std::string str) {
	int total = 0;
	for(unsigned int i = 0; i < str.size(); ++i) {
		FT_UInt glyph_index = FT_Get_Char_Index(face, str[i]);
		if (FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT))
			throw new MError("failed to load glyph ");
		total += face->glyph->advance.x >> 6;
	}
	return total;
}
