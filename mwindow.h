#ifndef __MWINDOW_H__
#define __MWINDOW_H__

#include "mlayout.h"
#include "mapplication.h"
#include "mdrawable.h"

class MWindowImpl;

class MWindow : public MLayout {
public:
	MApplication *app;
	MDrawableFormat format; // TODO Does this even belong here?
	MWindowImpl *impl;

	MWindow(MApplication *a, int x, int y, unsigned int w, unsigned int h);
	~MWindow();

	void map();
	void unmap();

	virtual void on_close(bool irrevocable) {};

	virtual void invalidate();
	virtual void invalidate(MRect *rect);
	virtual void invalidate(int x, int y, int w, int h);
};

#endif /* __MWINDOW_H__ */
