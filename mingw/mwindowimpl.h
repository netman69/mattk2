#ifndef __MWINDOWIMPL_H__
#define __MWINDOWIMPL_H__

#include "../mwindow.h"
#include "mapplicationimpl.h"

#include <windows.h>

class MWindowImpl {
public:
	MDrawable *drawable;
	MWindow *mw;
	HWND hwnd;
	BITMAPINFO bmi;

	MWindowImpl(MWindow *win, int x, int y, int w, int h);
	~MWindowImpl();

	static LRESULT CALLBACK StaticWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);
	LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

	void new_drawable(unsigned int w, unsigned int h);
};

#endif /* __MWINDOWIMPL_H__ */
