#ifndef __MAPPLICATIONIMPL_H__
#define __MAPPLICATIONIMPL_H__

#include "../mwindow.h"

#include <list>

#include <windows.h>

class MApplicationImpl { /* For one display and screen, open :0.1 for second screen, etc. */
public:
	std::list<MWindow *> windows;

	static HINSTANCE hInstance;
};

#endif /* __MAPPLICATIONIMPL_H__ */
