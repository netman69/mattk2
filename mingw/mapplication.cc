#include "../mapplication.h"
#include "mapplicationimpl.h"
#include "../mwindow.h"
#include "mwindowimpl.h"
#include "../mtimer.h"
#include "../merror.h"
#include "../mdrawableformat.h"

#include <list>
#include <algorithm> /* std::find */

//struct timeval tv;
#include <windows.h>

HINSTANCE MApplicationImpl::hInstance;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	MApplicationImpl::hInstance = hInstance;
	return mat_main(0, NULL); // TODO arguments
}

MApplication::MApplication(int argc, char *argv[]) {
	impl = new MApplicationImpl();
}

MApplication::~MApplication() {
	//XCloseDisplay(impl->dpy);
	delete impl;
}

int MApplication::run() {
	MSG msg;
	UINT_PTR ctimer = 0;
	/* We tell all windows to draw here, we can not do it in the constructor of them because overloads functions won't be called from those. */
	for(std::list<MWindow *>::iterator i = impl->windows.begin(); i != impl->windows.end(); ++i) {
		// TODO!
		//XWindowAttributes attr;
		//XGetWindowAttributes(impl->dpy, (*i)->impl->win, &attr);
		//MRect rect(attr.x, attr.y, attr.x + attr.width, attr.y + attr.height);
		//(*i)->pos = MRect(&rect); /* Must set this the first time, in case layout depends on previous values. */
		//(*i)->moveresize(&rect);
		(*i)->invalidate();
	}
	/* Now the main loop. */
	while(GetMessage(&msg, NULL, 0, 0) > 0) {
		/* Check when the next timer is expiring. */
		unsigned long time = MTimer::get_time();
		long nextexpiry = 0x7FFFFFFF; /* Basically forever from now */
		for(std::list<MTimer *>::iterator i = timers.begin(); i != timers.end(); ++i) {
			long distance = ((long) (*i)->duration) - ((long) (time - (*i)->started));
			if (distance < nextexpiry)
				nextexpiry = distance;
		}
		if (nextexpiry < 0)
			nextexpiry = 0;
		KillTimer(NULL, ctimer);
		ctimer = SetTimer(NULL, 0, nextexpiry, NULL);
		/* Run any expired timers */
		time = MTimer::get_time();
		std::list<MTimer *> timers = this->timers; /* We copy the list in case an item appears/disappears inside a timer callback. */
		for (std::list<MTimer *>::iterator i = timers.begin(); i != timers.end(); ++i) {
			if (time - (*i)->started >= (*i)->duration && *std::find(this->timers.begin(), this->timers.end(), *i) == *i)
				(*i)->trigger(); /* We also check if the item still exists before this. */
		}
		/* Handle windows events. */
		if (msg.message == WM_TIMER)
			continue;
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
	return msg.wParam;
}
