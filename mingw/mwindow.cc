#include "../mwindow.h"
#include "mwindowimpl.h"
#include "../mapplication.h"
#include "mapplicationimpl.h"
#include "../mdrawable.h"
#include "../merror.h"

#include <stdlib.h> /* NULL */

const char g_szClassName[] = "myWindowClass"; // TODO Must be different for each window (or not?).

LRESULT CALLBACK MWindowImpl::StaticWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	MWindowImpl *win;
	if (msg == WM_NCCREATE) {
		win = (MWindowImpl *) ((CREATESTRUCT *) lParam)->lpCreateParams;
		SetLastError(0);
		if (!SetWindowLongPtr(hwnd, GWL_USERDATA, (LONG_PTR) win) && GetLastError() != 0)
			return FALSE;
	} else {
		win = (MWindowImpl *) GetWindowLongPtr(hwnd, GWL_USERDATA);
		if (win != NULL)
			return win->WndProc(hwnd, msg, wParam, lParam);
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

LRESULT CALLBACK MWindowImpl::WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		case WM_CLOSE:
			DestroyWindow(hwnd);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		case WM_PAINT:
			{
				RECT rect;
				GetClientRect(hwnd, &rect);
				PAINTSTRUCT ps;
				HDC hdc = BeginPaint(hwnd, &ps); 
				// TODO Partial redrawing.
				SetDIBitsToDevice(hdc, 0, 0, drawable->w, drawable->h, 0, 0, 0, drawable->h, drawable->data, &bmi, DIB_RGB_COLORS);
				//StretchDIBits(hdc, 0, 0, drawable->w, drawable->h, 0, 0, drawable->w, drawable->h, drawable->data, &bmi, DIB_RGB_COLORS, SRCCOPY);
				EndPaint(hwnd, &ps); 
			}
			break;
		case WM_SIZE:
			{
				// TODO In X11 the similar event sends moveresize straight off, here we need separate one for move still.
				RECT rect;
				//GetWindowRect(hwnd, &rect);
				GetClientRect(hwnd, &rect);
				/*if (drawable != NULL && rect.right == drawable->w && rect.bottom == drawable->h)
					break;*/
				MRect newpos(rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top);
				mw->moveresize(&newpos); // TODO position incorrect
				new_drawable(newpos.w, newpos.h);
				// TODO do we need to do similar to this?:
				//XQueryPointer(mw->app->impl->dpy, win, &dw, &dw, &d, &d, &x, &y, (unsigned int *) &d);
				//mw->on_pointer_move(x, y);
				//printf("%d %d %llx\n", drawable->w, drawable->h, drawable);
				mw->invalidate();
			}
			break;
		default:
			return DefWindowProc(hwnd, msg, wParam, lParam);
	}
	return 0L;
}

MWindowImpl::MWindowImpl(MWindow *win, int x, int y, int w, int h) {
	mw = win;
	drawable = NULL;
	WNDCLASSEX wc;

	//Step 1: Registering the Window Class
	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = WS_OVERLAPPEDWINDOW;
	wc.lpfnWndProc = MWindowImpl::StaticWndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = MApplicationImpl::hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = 0; // TODO unneeded later
	wc.lpszMenuName = NULL;
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return;
	}

	// Step 2: Creating the Window
	RECT sz = { .left = 0, .top = 0, .right = w, .bottom = h };
	AdjustWindowRectEx(&sz, wc.style, FALSE, 0);
	// TODO CW_USEDEFAULT can be used for default window pos/size.
	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE, g_szClassName, "", WS_OVERLAPPEDWINDOW, x, y, sz.right - sz.left, sz.bottom - sz.top, NULL, NULL, MApplicationImpl::hInstance, this);
	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return;
	}
	LONG lExStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
	lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
	SetWindowLong(hwnd, GWL_EXSTYLE, lExStyle);
}

MWindowImpl::~MWindowImpl() {
/*	XDestroyWindow(mw->app->impl->dpy, win);
	XFree(image);*/ /* XDestroyImage deletes the data too. */
	delete drawable;
}

void MWindowImpl::new_drawable(unsigned int w, unsigned int h) {
	int lw = ((w * 3) & ~3) + (((w * 3) & 3) ? 4 : 0); /* Aligning is important */
	delete drawable;
	mw->format = MDrawableFormat(MColorFormat::RGB888, 0, lw);
	drawable = new MDrawable(w, h, &mw->format);
	ZeroMemory(&bmi, sizeof(BITMAPINFO));
	bmi.bmiHeader.biBitCount = 24;
	bmi.bmiHeader.biWidth = w;
	bmi.bmiHeader.biHeight = -h;
	bmi.bmiHeader.biPlanes = 1;
	bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmi.bmiHeader.biSizeImage = h * lw;
	bmi.bmiHeader.biCompression = BI_RGB;
}

/*bool MWindowImpl::event_handler(XEvent *e) {
	if (e->xany.window != win)
		return false;
	switch(e->type) {
		case ConfigureNotify:
			while (XCheckTypedWindowEvent(mw->app->impl->dpy, win, ConfigureNotify, e));
			{
				MRect newpos(e->xconfigure.x, e->xconfigure.y, e->xconfigure.width, e->xconfigure.height);
				mw->moveresize(&newpos);
				if (e->xconfigure.width == drawable->w && e->xconfigure.height == drawable->h)
					return true;
				new_drawable(e->xconfigure.width, e->xconfigure.height);
				int x, y, d;
				Window dw;
				XQueryPointer(mw->app->impl->dpy, win, &dw, &dw, &d, &d, &x, &y, (unsigned int *) &d);
				mw->on_pointer_move(x, y);
				mw->invalidate();
			}
			return true;
		case Expose:
			XPutImage(mw->app->impl->dpy, win, mw->app->impl->gc, image, e->xexpose.x, e->xexpose.y, e->xexpose.x, e->xexpose.y, e->xexpose.width, e->xexpose.height);
			return true;
		case DestroyNotify:
			mw->on_close(true);
			return true;
		case ClientMessage:
			if ((Atom) e->xclient.data.l[0] == mw->app->impl->atom_wm_delete_window)
				mw->on_close(false);
			return true;
		case MotionNotify:
			mw->on_pointer_move(e->xmotion.x, e->xmotion.y);
			return true;
		case ButtonPress:
			mw->on_button_press(e->xbutton.button);
			return true;
		case ButtonRelease:
			mw->on_button_release(e->xbutton.button);
			return true;
	}
	return false;
}*/

MWindow::MWindow(MApplication *a, int x, int y, unsigned int w, unsigned int h) {
	app = a;
	a->impl->windows.push_front(this);
	impl = new MWindowImpl(this, x, y, w, h);
}

MWindow::~MWindow() {
	app->impl->windows.remove(this);
	delete impl;
}

void MWindow::map() {
	// We can't call SetWindowPos in constructor, cause it calls WndProc.
	SetWindowPos(impl->hwnd, NULL, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED);
	ShowWindow(impl->hwnd, SW_SHOWNORMAL);
	//invalidate(); // TODO Don't know if necessary.
}

void MWindow::unmap() {
	ShowWindow(impl->hwnd, SW_HIDE);
}

void MWindow::invalidate() {
	if ((int)impl->drawable > 0) { // TODO wtf
		draw(impl->drawable);
		RECT ur;
		ur.left = 0;
		ur.top = 0;
		ur.right = impl->drawable->w;
		ur.bottom = impl->drawable->h;
		InvalidateRect(impl->hwnd, &ur, FALSE);
	}
	//UpdateWindow(impl->hwnd);
}

void MWindow::invalidate(MRect* rect) {
	MRect r = *rect;
	r.fit(&impl->drawable->clip);
	draw(impl->drawable, &r);
	UpdateWindow(impl->hwnd); // TODO RedrawWindow instead, or maybe only invalidaterect is needed?
	//XPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, r.x, r.y, r.x, r.y, r.w, r.h);
}

void MWindow::invalidate(int x, int y, int w, int h) {
	MRect rect(x, y, w, h);
	invalidate(&rect);
}
