#ifndef __MLAYOUT_H__
#define __MLAYOUT_H__

#include "mwidget.h"
#include "mdrawable.h"

#include <list>
#include <vector>

class MLayoutButtonCapture {
public:
	int button;
	MWidget *w;

	MLayoutButtonCapture(int button, MWidget *w);
};

class MLayout : public MWidget {
public:
	std::list<MWidget *> widgets;
	std::vector<MLayoutButtonCapture> captures;

	void on_pointer_move(int x, int y);
	void on_button_press(int button);
	void on_button_release(int button);

	void moveresize(MRect *newpos);

	virtual void draw(MDrawable *d);
	virtual void draw(MDrawable *d, MRect *rect);

	virtual void invalidate() {};
	virtual void invalidate(MRect *rect) {};
	virtual void invalidate(int x, int y, int w, int h) {};
};

#endif /* __MLAYOUT_H__ */
