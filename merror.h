#ifndef __MERROR_H__
#define __MERROR_H__

#include <string>

class MError {
public:
	std::string message;
	explicit MError(std::string msg);
};

#endif /* __MERROR_H__ */
