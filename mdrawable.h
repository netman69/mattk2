#ifndef __MDRAWABLE_H__
#define __MDRAWABLE_H__

#include "mdrawableformat.h"
#include "mcolor.h"

#include <string>

class MRect {
public:
	int x, y, w, h;

	MRect();
	MRect(int x, int y, int w, int h);
	explicit MRect(MRect *rect);

	void trim_inversion(); /* If width or height negative, make it 0. */
	void fit(MRect *rect); /* Make it smaller until it fits given rect. */
	void move(int x, int y); /* Move it so x0 and y0 correspond to given x and y. */
	void translate(int x, int y); /* Shift by x and y. */
	bool is_inverted(); /* Tell w or h is negative. */
	bool is_inside(int x, int y);
	bool is_overlap(MRect *rect);
	bool has_size();

	int x1();
	int y1();
};

class MDrawable {
public:
	MDrawableFormat format;
	unsigned char *data;
	int w, h;
	MRect clip;
	int pitch;
	MColor color;

	MDrawable(unsigned int w, unsigned int h, MDrawableFormat *format, bool alloc = true);
	MDrawable(MDrawable *d, int x0, int y0, int x1, int y1); /* Create sub-drawable that is only valid as long as data of this one is valid. */
	MDrawable(MDrawable *d, MRect *rect);
	~MDrawable();

	void draw(MDrawable *d, int x, int y);

	void draw_point(int x, int y);
	void draw_line(int x0, int y0, int x1, int y1);
	void draw_line_aa(int x0, int y0, int x1, int y1);
	void draw_rect(int x0, int y0, int x1, int y1);
	void draw_rect(MRect *rect);

	void clear(); /* Fill with current color, overwriting alpha. */
	void fill(bool overwrite_alpha = false); /* Fill with current color. */
	void fill_rect(int x0, int y0, int x1, int y1, bool overwrite_alpha = false);
	void fill_rect(MRect *rect, bool overwrite_alpha = false);

private:
	bool data_owned;

	void init(MDrawable *d, MRect *rect);
	void draw_point_na(int x, int y); /* No alpha. */
	void draw_point_pm(int x, int y); /* Premultiplied alpha. */
	void fill_rect_alpha(int x0, int y0, int w, int h);
};

#endif /* __MDRAWABLE_H__ */
