#include "mtimer.h"
#include "mapplication.h"

#include <stdlib.h> /* NULL */
#include <sys/time.h>

MTimer::MTimer(MApplication *a, MTimerEvent *tev, unsigned long duration) {
	app = a;
	this->duration = duration;
	this->repeat = true;
	this->tev = tev;
	this->started = 0;
}

MTimer::MTimer(MApplication *a, MTimerEvent *tev, unsigned long duration, bool repeat) {
	app = a;
	this->duration = duration;
	this->repeat = repeat;
	this->tev = tev;
	this->started = 0;
}

MTimer::~MTimer() {
	stop();
}

unsigned long MTimer::get_time() {
	struct timeval  tv;
	gettimeofday(&tv, NULL);
	/* I don't think it matters this can overflow */
	return ((unsigned long) tv.tv_usec) / 1000UL + ((unsigned long) tv.tv_sec) * 1000UL;
}

void MTimer::start() {
	started = get_time();
	app->timers.push_front(this);
}

void MTimer::stop() {
	app->timers.remove(this);
}

void MTimer::trigger() {
	if (!repeat)
		stop();
	else started = get_time();
	tev->timer_event();
}
