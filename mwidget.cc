#include "mwidget.h"
#include "mlayout.h"

#include <stdlib.h> /* NULL */

MWidget::MWidget(MLayout *layout) {
	this->layout = layout;
	if (layout != NULL)
		layout->widgets.push_back(this);
	pointer_inside = false;
	anchors = MAnchor::LEFT | MAnchor::TOP;
}

MWidget::~MWidget() {
	pointer_inside = false;
	if (layout != NULL)
		layout->widgets.remove(this);
}

void MWidget::moveresize(MRect *newpos) {
	pos = MRect(newpos);
}

void MWidget::moveresize(int x, int y, int w, int h) {
	MRect rect(x, y, w, h);
	moveresize(&rect);
}

void MWidget::draw(MDrawable* d) {
	MRect rect(0, 0, d->w, d->h);
	draw(d, &rect);
}

void MWidget::draw(MDrawable* d, MRect* rect) {
	draw(d);
}

void MWidget::invalidate() {
	if (layout != NULL)
		layout->invalidate(&pos);
}

void MWidget::invalidate(MRect *rect) {
	if (layout != NULL) {
		MRect r = *rect;
		r.move(pos.x, pos.y);
		layout->invalidate(&pos);
	}
}

void MWidget::invalidate(int x, int y, int w, int h) {
	MRect rect(x, y, w, h);
	invalidate(&rect);
}
