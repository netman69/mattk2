#include "../mapplication.h"
#include "mapplicationimpl.h"
#include "../mwindow.h"
#include "mwindowimpl.h"
#include "../mtimer.h"
#include "../merror.h"
#include "../mdrawableformat.h"

#include <list>
#include <algorithm> /* std::find */

/* All for select() and related macros/types */
#include <sys/select.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <X11/Xlib.h>

struct timeval tv;

/* Because windows has winmain, etc. */
int main(int argc, char *argv[]) {
	return mat_main(argc, argv);
}

MApplication::MApplication(int argc, char *argv[]) {
	impl = new MApplicationImpl();
	char *display_name = NULL;
	/* TODO get display name from arguments */
	if ((impl->dpy = XOpenDisplay(display_name)) == NULL)
		throw MError("can't open display " + std::string(display_name));
	impl->screen = DefaultScreen(impl->dpy);
	impl->depth = DefaultDepth(impl->dpy, impl->screen);
	impl->root = RootWindow(impl->dpy, impl->screen);
	impl->gc = DefaultGC(impl->dpy, impl->screen);
	impl->visual = DefaultVisual(impl->dpy, impl->screen);
	impl->atom_wm_delete_window = XInternAtom(impl->dpy, "WM_DELETE_WINDOW", False);
}

MApplication::~MApplication() {
	XCloseDisplay(impl->dpy);
	delete impl;
}

int MApplication::run() {
	int xfd = ConnectionNumber(impl->dpy);
	fd_set in_fds;
	struct timeval tv;
	/* We tell all windows to draw here, we can not do it in the constructor of them because overloads functions won't be called from those. */
	for(std::list<MWindow *>::iterator i = impl->windows.begin(); i != impl->windows.end(); ++i) {
		XWindowAttributes attr;
		XGetWindowAttributes(impl->dpy, (*i)->impl->win, &attr);
		MRect rect(attr.x, attr.y, attr.x + attr.width, attr.y + attr.height);
		(*i)->pos = MRect(&rect); /* Must set this the first time, in case layout depends on previous values. */
		(*i)->moveresize(&rect);
		(*i)->invalidate(); // TODO Is this even necessary?
	}
	/* Now the main loop. */
	XEvent e;
	while (1) {
		/* Handle X11 events. */
		while(XPending(impl->dpy)) {
			XNextEvent(impl->dpy, &e);
			std::list<MWindow *> windows = impl->windows; /* Copy the list in case the event handler removes the window. */
			for(std::list<MWindow *>::iterator i = windows.begin(); i != windows.end(); ++i)
				if (*std::find(impl->windows.begin(), impl->windows.end(), *i) == *i && (*i)->impl->event_handler(&e))
					break;
		}
		XFlush(impl->dpy);

		/* Prepare FD set. */
		FD_ZERO(&in_fds);
		FD_SET(xfd, &in_fds);

		unsigned long time = MTimer::get_time();
		long nextexpiry = 0x7FFFFFFF; /* Basically forever from now */
		for(std::list<MTimer *>::iterator i = timers.begin(); i != timers.end(); ++i) {
			long distance = ((long) (*i)->duration) - ((long) (time - (*i)->started));
			if (distance < nextexpiry)
				nextexpiry = distance;
		}
		if (nextexpiry < 0)
			nextexpiry = 0;

		tv.tv_sec = nextexpiry / 1000;
		tv.tv_usec = (nextexpiry % 1000) * 1000;

		/* Wait until timeout or X11 data. */
		select(xfd + 1, &in_fds, NULL, NULL, &tv);

		/* Run any expired timers */
		time = MTimer::get_time();
		std::list<MTimer *> timers = this->timers; /* We copy the list in case an item appears/disappears inside a timer callback. */
		for (std::list<MTimer *>::iterator i = timers.begin(); i != timers.end(); ++i) {
			if (time - (*i)->started >= (*i)->duration && *std::find(this->timers.begin(), this->timers.end(), *i) == *i)
				(*i)->trigger(); /* We also check if the item still exists before this. */
		}
	}
	return 0;
}
