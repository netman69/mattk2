#ifndef __MAPPLICATIONIMPL_H__
#define __MAPPLICATIONIMPL_H__

#include "../mwindow.h"

#include <X11/Xlib.h>

#include <list>

class MApplicationImpl { /* For one display and screen, open :0.1 for second screen, etc. */
public:
	Display *dpy;
	int screen, depth;
	Visual *visual;
	GC gc;
	Window root;
	std::list<MWindow *> windows;
	Atom atom_wm_delete_window;
};

#endif /* __MAPPLICATIONIMPL_H__ */
