#include "../mwindow.h"
#include "mwindowimpl.h"
#include "../mapplication.h"
#include "mapplicationimpl.h"
#include "../mdrawable.h"
#include "../merror.h"

#include <stdlib.h> /* NULL */

MWindowImpl::MWindowImpl(MWindow *w) {
	mw = w;
#ifndef NO_SHM
	shm_ok = false; /* TODO Option to turn off MIT-SHM. */
	if(XShmQueryExtension(mw->app->impl->dpy))
		shm_ok = true;
#endif
	drawable = NULL;
	image = NULL;
}

MWindowImpl::~MWindowImpl() {
#ifndef NO_SHM
	if (shm_ok && drawable != NULL) {
		XShmDetach(mw->app->impl->dpy, &shm_info);
		XSync(mw->app->impl->dpy, False); /* Make sure we are detached, but not sure if necessary. */
		shmctl(shm_info.shmid, IPC_RMID, 0);
		shmdt(shm_info.shmaddr);
	}
#endif
	XDestroyWindow(mw->app->impl->dpy, win);
	XFree(image); /* XDestroyImage deletes the data too. */
	delete drawable;
}

void MWindowImpl::new_drawable(unsigned int w, unsigned int h) {
	/* The before last argument, is the integer multiple of bits at which the image is broken into scanlines. */
	/* We pass NULL as the data, and later fill the data field of the XImage structure. */
	XFree(image);
#ifndef NO_SHM
	if (shm_ok)
		image = XShmCreateImage(mw->app->impl->dpy, mw->app->impl->visual, mw->app->impl->depth, ZPixmap, NULL, &shm_info, w, h);
	else image = XCreateImage(mw->app->impl->dpy, mw->app->impl->visual, mw->app->impl->depth, ZPixmap, 0, (char *) NULL, w, h, 8, 0);
#else
	image = XCreateImage(mw->app->dpy, mw->app->visual, mw->app->depth, ZPixmap, 0, (char *) NULL, w, h, 8, 0);
#endif

	switch (image->bits_per_pixel) {
		case 32:
			if (image->red_mask == 0xFF000000)
				mw->format = MDrawableFormat(MColorFormat::RGBA8888, 0, w * 4);
			else if (image->red_mask == 0x00FF0000)
				mw->format = MDrawableFormat(MColorFormat::ARGB8888, 0, w * 4);
			else if (image->red_mask == 0x0000FF00)
				mw->format = MDrawableFormat(MColorFormat::BGRA8888, 0, w * 4);
			else if (image->red_mask == 0x000000FF)
				mw->format = MDrawableFormat(MColorFormat::ABGR8888, 0, w * 4);
			else throw MError("unsupported color mode");
			break;
		case 24:
			if (image->red_mask == 0x00FF0000)
				mw->format = MDrawableFormat(MColorFormat::RGB888, 0, w * 3);
			else if (image->red_mask == 0x000000FF)
				mw->format = MDrawableFormat(MColorFormat::BGR888, 0, w * 3);
			else if (image->red_mask == 0xFF000000)
				mw->format = MDrawableFormat(MColorFormat::RGB888L, 0, w * 3);
			else if (image->red_mask == 0x0000FF00)
				mw->format = MDrawableFormat(MColorFormat::BGR888L, 0, w * 3);
			else throw MError("unsupported color mode");
			break;
		case 16:
			if (image->green_mask != 0x07E0)
				throw MError("unsupported color mode");
			if (image->red_mask == 0xF800)
				mw->format = MDrawableFormat(MColorFormat::RGB565, 0, w * 2);
			else if (image->red_mask == 0x01F)
				mw->format = MDrawableFormat(MColorFormat::BGR565, 0, w * 2);
			else throw MError("unsupported color mode");
			break;
		default:
			throw MError("unsupported color mode");
	}
#ifndef NO_SHM
	if (shm_ok) {
		if (drawable != NULL) {
			XShmDetach(mw->app->impl->dpy, &shm_info);
			XSync(mw->app->impl->dpy, False); /* Make sure we are detached, but not sure if necessary. */
			shmctl(shm_info.shmid, IPC_RMID, 0);
			shmdt(shm_info.shmaddr);
		}
		delete drawable;
		drawable = new MDrawable(w, h, &mw->format, false);
        shm_info.shmid = shmget(IPC_PRIVATE, h * drawable->pitch, IPC_CREAT | 0777);
		shm_info.shmaddr = (char *) shmat(shm_info.shmid, NULL, 0);
		shm_info.readOnly = True;
		XShmAttach(mw->app->impl->dpy, &shm_info);
		if (shm_info.shmid == -1 || shm_info.shmaddr == (void *) -1)
			throw new MError("failed to get shared memory");
		image->data = shm_info.shmaddr;
		drawable->data = (unsigned char *) image->data;
	} else {
		delete drawable;
		drawable = new MDrawable(w, h, &mw->format);
		image->data = (char *) drawable->data;
	}
#else
	delete drawable;
	drawable = new MDrawable(w, h, &mw->format);
	image->data = (char *) drawable->data;
#endif
}

bool MWindowImpl::event_handler(XEvent *e) {
	if (e->xany.window != win)
		return false;
	switch(e->type) {
		case ConfigureNotify:
			while (XCheckTypedWindowEvent(mw->app->impl->dpy, win, ConfigureNotify, e));
			{
				MRect newpos(e->xconfigure.x, e->xconfigure.y, e->xconfigure.width, e->xconfigure.height);
				mw->moveresize(&newpos);
				if (e->xconfigure.width == drawable->w && e->xconfigure.height == drawable->h)
					return true;
				new_drawable(e->xconfigure.width, e->xconfigure.height);
				int x, y, d;
				Window dw;
				XQueryPointer(mw->app->impl->dpy, win, &dw, &dw, &d, &d, &x, &y, (unsigned int *) &d);
				mw->on_pointer_move(x, y);
				mw->invalidate();
			}
			return true;
		case Expose:
			XPutImage(mw->app->impl->dpy, win, mw->app->impl->gc, image, e->xexpose.x, e->xexpose.y, e->xexpose.x, e->xexpose.y, e->xexpose.width, e->xexpose.height);
			return true;
		case DestroyNotify:
			mw->on_close(true);
			return true;
		case ClientMessage:
			if ((Atom) e->xclient.data.l[0] == mw->app->impl->atom_wm_delete_window)
				mw->on_close(false);
			return true;
		case MotionNotify:
			mw->on_pointer_move(e->xmotion.x, e->xmotion.y);
			return true;
		case ButtonPress:
			mw->on_button_press(e->xbutton.button);
			return true;
		case ButtonRelease:
			mw->on_button_release(e->xbutton.button);
			return true;
	}
	return false;
}

MWindow::MWindow(MApplication *a, int x, int y, unsigned int w, unsigned int h) {
	app = a;
	a->impl->windows.push_front(this);
	impl = new MWindowImpl(this);
	XSetWindowAttributes attr;
	attr.bit_gravity = StaticGravity; /* Tells X not to clear the window on resize, reducing flicker. */
	attr.background_pixel = 0;
	attr.event_mask = StructureNotifyMask | ExposureMask | PointerMotionMask | ButtonPressMask | ButtonReleaseMask;
	impl->win = XCreateWindow(app->impl->dpy, app->impl->root, x, y, w, h, 0, app->impl->depth, InputOutput, app->impl->visual, CWBitGravity | CWBackPixel | CWEventMask, &attr);
	impl->new_drawable(w, h);
	XSetWMProtocols(app->impl->dpy, impl->win, &app->impl->atom_wm_delete_window, 1);
}

MWindow::~MWindow() {
	app->impl->windows.remove(this);
	delete impl;
}

void MWindow::map() {
	XMapWindow(app->impl->dpy, impl->win);
}

void MWindow::unmap() {
	XUnmapWindow(app->impl->dpy, impl->win);
}

void MWindow::invalidate() {
	draw(impl->drawable);
#ifndef NO_SHM
	if (impl->shm_ok)
		XShmPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, 0, 0, 0, 0, impl->drawable->w, impl->drawable->h, True);
	else XPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, 0, 0, 0, 0, impl->drawable->w, impl->drawable->h);
#else
	XPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, 0, 0, 0, 0, impl->drawable->w, impl->drawable->h);
#endif
}

void MWindow::invalidate(MRect* rect) {
	MRect r = *rect;
	r.fit(&impl->drawable->clip);
	draw(impl->drawable, &r);
#ifndef NO_SHM
	if (impl->shm_ok)
		XShmPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, r.x, r.y, r.x, r.y, r.w, r.h, False);
	else XPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, r.x, r.y, r.x, r.y, r.w, r.h);
#else
	XPutImage(app->impl->dpy, impl->win, app->impl->gc, impl->image, r.x, r.y, r.x, r.y, r.w, r.h);
#endif
}

void MWindow::invalidate(int x, int y, int w, int h) {
	MRect rect(x, y, w, h);
	invalidate(&rect);
}
