#ifndef __MWINDOWIMPL_H__
#define __MWINDOWIMPL_H__

#include "../mwindow.h"
#include "mapplicationimpl.h"

#include <X11/Xlib.h>
#ifndef NO_SHM
   #include <sys/ipc.h>
   #include <sys/shm.h>
   #include <X11/extensions/XShm.h>
#endif

class MWindowImpl {
public:
	MDrawable *drawable;
	MWindow *mw;
	Window win;
	XImage *image;
#ifndef NO_SHM
	XShmSegmentInfo shm_info;
	bool shm_ok;
#endif

	explicit MWindowImpl(MWindow *w);
	~MWindowImpl();

	void new_drawable(unsigned int w, unsigned int h);
	bool event_handler(XEvent *e);
};

#endif /* __MWINDOWIMPL_H__ */
