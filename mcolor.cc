#include "mcolor.h"
#include "merror.h"

MColor::MColor() {
	this->r = 0;
	this->g = 0;
	this->b = 0;
	this->a = 0xFF;
	format = MColorFormat::NONE;
	value = 0;
}

MColor::MColor(unsigned char r, unsigned char g, unsigned char b) {
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = 0xFF;
	format = MColorFormat::NONE;
	value = 0;
}

MColor::MColor(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
	format = MColorFormat::NONE;
	value = 0;
}

MColor::MColor(MColor *color) {
	this->r = color->r;
	this->g = color->g;
	this->b = color->b;
	this->a = color->a;
	format = MColorFormat::NONE;
	value = 0;
}

void MColor::set_format(int format) {
	this->format = format;
	value = get(format);
}

void MColor::set(unsigned char r, unsigned char g, unsigned char b) {
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = 0xFF;
	value = get(format);
}

void MColor::set(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
	value = get(format);
}

void MColor::set(MColor *color) {
	this->r = color->r;
	this->g = color->g;
	this->b = color->b;
	this->a = color->a;
	value = get(format);
}

void MColor::set_a(unsigned char a) {
	this->a = a;
	value = get(format);
}

void MColor::blend(MColor *color) {
	unsigned int m1 = 0xFF - color->a;
	this->r = ((unsigned int) this->r * m1 + color->r * color->a) / 0xFF;
	this->g = ((unsigned int) this->g * m1 + color->g * color->a) / 0xFF;
	this->b = ((unsigned int) this->b * m1 + color->b * color->a) / 0xFF;
	this->a = (unsigned int) this->a * m1 / 0xFF + color->a;
	value = get(format);
}

void MColor::blend_pm(MColor *color) {
	unsigned int m1 = 0xFF - color->a;
	this->r = (unsigned int) this->r * m1 / 0xFF + color->r;
	this->g = (unsigned int) this->g * m1 / 0xFF + color->g;
	this->b = (unsigned int) this->b * m1 / 0xFF + color->b;
	this->a = (unsigned int) this->a * m1 / 0xFF + color->a;
	value = get(format);
}

unsigned int MColor::get(int format) {
	switch (format) {
		case MColorFormat::RGBA8888:
			return (r << 24) | (g << 16) | (b << 8) | a;
		case MColorFormat::ARGB8888:
			return (a << 24) | (r << 16) | (g << 8) | b;
		case MColorFormat::BGRA8888:
			return (b << 24) | (g << 16) | (r << 8) | a;
		case MColorFormat::ABGR8888:
			return (a << 24) | (b << 16) | (g << 8) | r;
		case MColorFormat::RGB888:
			return (r << 16) | (g << 8) | b;
		case MColorFormat::BGR888:
			return (b << 16) | (g << 8) | r;
		case MColorFormat::RGB888L:
			return (r << 24) | (g << 16) | (b << 8);
		case MColorFormat::BGR888L:
			return (b << 24) | (g << 16) | (r << 8);
		case MColorFormat::RGB565:
			return (r >> 3 << 11) | (g >> 2 << 5) | (b >> 3 << 0);
		case MColorFormat::BGR565:
			return (b >> 3 << 11) | (g >> 2 << 5) | (r >> 3 << 0);
		default:
			return 0;
	}
}

void MColor::read(unsigned int c) {
	read(c, format);
}

void MColor::read(unsigned int c, int format) {
	switch (format) {
		case MColorFormat::RGBA8888:
			r = (c >> 24);
			g = (c >> 16);
			b = (c >> 8);
			a = (c >> 0);
			break;
		case MColorFormat::RGB888L:
			r = (c >> 24);
			g = (c >> 16);
			b = (c >> 8);
			a = 0xFF;
			break;
		case MColorFormat::ARGB8888:
			a = (c >> 24);
			r = (c >> 16);
			g = (c >> 8);
			b = (c >> 0);
			break;
		case MColorFormat::RGB888:
			r = (c >> 16);
			g = (c >> 8);
			b = (c >> 0);
			a = 0xFF;
			break;
		case MColorFormat::BGRA8888:
			b = (c >> 24);
			g = (c >> 16);
			r = (c >> 8);
			a = (c >> 0);
			break;
		case MColorFormat::BGR888L:
			b = (c >> 24);
			g = (c >> 16);
			r = (c >> 8);
			a = 0xFF;
			break;
		case MColorFormat::ABGR8888:
			b = (c >> 16);
			g = (c >> 8);
			r = (c >> 0);
			a = (c >> 24);
			break;
		case MColorFormat::BGR888:
			b = (c >> 16);
			g = (c >> 8);
			r = (c >> 0);
			a = 0xFF;
			break;
		case MColorFormat::RGB565:
			r = (c >> 11 << 3);
			g = (c >> 5 << 2);
			b = (c >> 0 << 3);
			a = 0xFF;
			break;
		case MColorFormat::BGR565:
			b = (c >> 11 << 3);
			g = (c >> 5 << 2);
			r = (c >> 0 << 3);
			a = 0xFF;
			break;
		default:
			throw new MError("unknown color format");
	}
	value = get(format);
}
